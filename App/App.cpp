/*
 * Copyright (C) 2011-2019 Intel Corporation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   * Neither the name of Intel Corporation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */


#include <stdio.h>
#include <string.h>
#include <cassert>

# include <unistd.h>
# include <pwd.h>
#include  <math.h>
#include  <time.h>
#include  <stdlib.h>
#include  <stdio.h>
#include  <set>

#include<errno.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<unistd.h>

#define MAXLINE 4096


# define MAX_PATH FILENAME_MAX

#include "sgx_urts.h"
#include "App.h"
#include "Enclave_u.h"
#include "Test/test_msi.h"
#include "Tools/ezOptionParser.h"
#include "Tools/time-func.h"
#include "Server/server_core.h"

/* Global EID shared by multiple threads */
sgx_enclave_id_t global_eid = 0;

typedef struct _sgx_errlist_t {
    sgx_status_t err;
    const char *msg;
    const char *sug; /* Suggestion */
} sgx_errlist_t;

/* Error code returned by sgx_create_enclave */
static sgx_errlist_t sgx_errlist[] = {
    {
        SGX_ERROR_UNEXPECTED,
        "Unexpected error occurred.",
        NULL
    },
    {
        SGX_ERROR_INVALID_PARAMETER,
        "Invalid parameter.",
        NULL
    },
    {
        SGX_ERROR_OUT_OF_MEMORY,
        "Out of memory.",
        NULL
    },
    {
        SGX_ERROR_ENCLAVE_LOST,
        "Power transition occurred.",
        "Please refer to the sample \"PowerTransition\" for details."
    },
    {
        SGX_ERROR_INVALID_ENCLAVE,
        "Invalid enclave image.",
        NULL
    },
    {
        SGX_ERROR_INVALID_ENCLAVE_ID,
        "Invalid enclave identification.",
        NULL
    },
    {
        SGX_ERROR_INVALID_SIGNATURE,
        "Invalid enclave signature.",
        NULL
    },
    {
        SGX_ERROR_OUT_OF_EPC,
        "Out of EPC memory.",
        NULL
    },
    {
        SGX_ERROR_NO_DEVICE,
        "Invalid SGX device.",
        "Please make sure SGX module is enabled in the BIOS, and install SGX driver afterwards."
    },
    {
        SGX_ERROR_MEMORY_MAP_CONFLICT,
        "Memory map conflicted.",
        NULL
    },
    {
        SGX_ERROR_INVALID_METADATA,
        "Invalid enclave metadata.",
        NULL
    },
    {
        SGX_ERROR_DEVICE_BUSY,
        "SGX device was busy.",
        NULL
    },
    {
        SGX_ERROR_INVALID_VERSION,
        "Enclave version was invalid.",
        NULL
    },
    {
        SGX_ERROR_INVALID_ATTRIBUTE,
        "Enclave was not authorized.",
        NULL
    },
    {
        SGX_ERROR_ENCLAVE_FILE_ACCESS,
        "Can't open enclave file.",
        NULL
    },
};

/* Check error conditions for loading enclave */
void print_error_message(sgx_status_t ret)
{
    size_t idx = 0;
    size_t ttl = sizeof sgx_errlist/sizeof sgx_errlist[0];

    for (idx = 0; idx < ttl; idx++) {
        if(ret == sgx_errlist[idx].err) {
            if(NULL != sgx_errlist[idx].sug)
                printf("Info: %s\n", sgx_errlist[idx].sug);
            printf("Error: %s\n", sgx_errlist[idx].msg);
            break;
        }
    }
    
    if (idx == ttl)
    	printf("Error code is 0x%X. Please refer to the \"Intel SGX SDK Developer Reference\" for more details.\n", ret);
}

/* Initialize the enclave:
 *   Call sgx_create_enclave to initialize an enclave instance
 */
int initialize_enclave(void)
{
    sgx_status_t ret = SGX_ERROR_UNEXPECTED;
    
    /* Call sgx_create_enclave to initialize an enclave instance */
    /* Debug Support: set 2nd parameter to 1 */
    ret = sgx_create_enclave(ENCLAVE_FILENAME, SGX_DEBUG_FLAG, NULL, NULL, &global_eid, NULL);
    if (ret != SGX_SUCCESS) {
        print_error_message(ret);
        return -1;
    }

    return 0;
}


void setup(int n)
{
    int  listenfd, connfd;
    struct sockaddr_in  servaddr;
    char  buff[4096];
    int  size;

    if( (listenfd = socket(AF_INET, SOCK_STREAM, 0)) == -1 ){
        printf("create socket error: %s(errno: %d)\n",strerror(errno),errno);
    }

    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(6666);

    if( bind(listenfd, (struct sockaddr*)&servaddr, sizeof(servaddr)) == -1){
        printf("bind socket error: %s(errno: %d)\n",strerror(errno),errno);
    }

    if( listen(listenfd, 10) == -1){
        printf("listen socket error: %s(errno: %d)\n",strerror(errno),errno);
    }

    printf("======waiting for client's input======\n");
    for(int i=0;i<n;i++){
        if( (connfd = accept(listenfd, (struct sockaddr*)NULL, NULL)) == -1){
            printf("accept socket error: %s(errno: %d)",strerror(errno),errno);
            continue;
        }
        size = recv(connfd, buff, MAXLINE, 0);
        buff[size] = '\0';
        printf("the input of %dth client is:%s",i, buff);
        close(connfd);
    }
    close(listenfd);
}

/*find the minlen of each clients' the xxth bucket and change it to the first([0])*/
void find_min_change(size_t ***bucket,size_t **len_bucket,size_t n,size_t n_bucket,size_t max_set_len)
{
    for(int t=0;t<n_bucket;t++)
    {
        size_t min_pos = 0;
        for(size_t i=1; i<n; i++){
            if(len_bucket[i][t] < len_bucket[min_pos][t]){
                min_pos = i;
            }
        }
        /*
        *   test output code, comment in non-debug state
        */
        if(min_pos){
            size_t *temp;
            //size_t temp_l;
            
            temp=bucket[0][t];
            bucket[0][t]=bucket[min_pos][t];
            bucket[min_pos][t]=temp;

            // temp_l=len_bucket[0][t];
            // len_bucket[0][t]=len_bucket[min_pos][t];
            // len_bucket[min_pos][t]=temp_l;
            size_t a=len_bucket[0][t];
            size_t b=len_bucket[min_pos][t];  
            a = a ^ b;
            b = a ^ b;
            a = a ^ b;

        }
    }
}

void msi_out(size_t ***X,size_t **len,size_t *out,size_t n,size_t len_min,size_t n_bucket)
{
    /*
    *   variable initialization part
    */
    size_t a=1, b;
    printf("I reach the enclave program now!!!\n");

    /*
    *   core computation part
    */

    size_t sum_len=0;
    // size_t **y=new size_t *[n_bucket];
    for(int t=0;t<n_bucket;t++)            //对于所有用户的同一个桶（第一个桶），yi初始化为第一个用户这个桶（第一个桶）里的元素，先和它比
    {

        size_t min_bucket=len[0][t];
        size_t *y=new size_t[min_bucket];

        // for(int i=0;i<n_bucket;i++)
        //  y[i]=new size_t[min_bucket];


        for(size_t i=0; i<min_bucket; i++)
        {
            y[i] = X[0][t][i];
        }


        for(size_t k=1;k<n;k++)             //第二个用户该桶号（第一个桶）内的所有元素的y做差，看是否有0 
        {
            for(size_t i=0;i<min_bucket;i++)
            {   
                a= 1;
                for(size_t j=0;j<len[k][t];j++)
                {
                    b = (X[k][t][j] ^ y[i]);
                    b = (b >> 32) | ((b << 32) >> 32);
                    b = (b >> 16) | ((b << 48) >> 48);
                    b = (b >> 8)  | ((b << 56) >> 56);
                    b = (b >> 4)  | ((b << 60) >> 60);
                    b = (b >> 2)  | ((b << 62) >> 62);
                    b = (b >> 1)  | ((b << 63) >> 63);
                    a = a & b;
                }       
                y[i] = y[i]*(1 - a);                //这一组里有0,yi就会保存下所有用户该桶号里相同的元素
            }
        }
        for(int i=0;i<min_bucket;i++)
        {
            out[sum_len+i]=y[i];
            //printf("out[%d]:%d",i,y[i]);
        }

        sum_len+=min_bucket;


    }

}

/*
* For attestation part
*/
char debug = 0;
char verbose = 0;

/*to test the cost with SGX,just copy the code from Enclave.cpp*/

/* Application entry */
int SGX_CDECL main(int argc, char *argv[])
{
    Timer ts_main;
    ts_main.start();
    /* 
    *   Get the parameters from the command line 
    */
    ez::ezOptionParser opt;
    opt.example = "./app  -np 2 -nm 1000 -nf 1000 -mod 0 -nt 1 -pn 5000\n";
    opt.add(
        "2", // Default.
        0, // Required?
        1, // Number of args expected.
        0, // Delimiter if expecting multiple args.
        "number of parties (default: 2)", // Help description.
        "-np", // Flag token.
        "--n_parties" // Flag token.
    );
    opt.add(
        "1000", // Default.
        0, // Required?
        1, // Number of args expected.
        0, // Delimiter if expecting multiple args.
        "the maximum set's length (default: 1000)", // Help description.
        "-nm", // Flag token.
        "--max_set_len" // Flag token.
    );
    opt.add(
        "0", // Default.
        0, // Required?
        1, // Number of args expected.
        0, // Delimiter if expecting multiple args.
        "the size of set intersection where 0 means not predertermined (default: 0)", // Help description.
        "-nf", // Flag token.
        "--n_final_size" // Flag token.
    );
    opt.add(
        "0", // Default.
        0, // Required?
        1, // Number of args expected.
        0, // Delimiter if expecting multiple args.
        "the mode of size of set size_avg_mode=0, size_unbalanced_mode=1, size_rnd_mode=2 (default: 0)", // Help description.
        "-mod", // Flag token.
        "--size_set_mode" // Flag token.
    );
    opt.parse(argc, (const char**)argv);
    

    /*
    *   Variable initialization part
    *   @n : the number of participants
    *   @X : the array of set; each row is a set with size of len[i] elements
    *   @y : the intersection of the set
    *   @intersection_set : cross validiation intersection of set
    *   @max_set_len : the maximum set length of set
    *   @size_final : the size of set intersection where 0 means not predertermined
    *   @mode : the mode of size of set size_avg_mode=0, size_unbalanced_mode=1, size_rnd_mode=2 (default: 0)
    */
    int n;
    opt.get("--n_parties")->getInt(n);
    size_t **X = new size_t* [n];
    size_t *len = new size_t[n];
    size_t *out = NULL;
    std::set<size_t> intersection_set;
    size_t max_set_len;
    size_t size_final;
    int tmp_int;
    set_length_mode mode; 

    //setup(n);

    /* 
    *   Initialize the enclave 
    */
    if(initialize_enclave() < 0){
        getchar();
        return -1; 
    } 

    /********************************************Generate the random set with the specific parameter Start ***************************/
    opt.get("--max_set_len")->getInt(tmp_int);
    custom_assert(tmp_int > 0);
    max_set_len = (size_t)tmp_int;
    
    opt.get("--n_final_size")->getInt(tmp_int);
    custom_assert(tmp_int >= 0);
    size_final = (size_t)tmp_int;
    
    opt.get("--size_set_mode")->getInt(tmp_int);
    //custom_assert(tmp_int >= 0);
    mode = (set_length_mode)tmp_int;
    //printf("mode:%d\n",mode);

    size_t mem_size = sizeof(size_t)*10 * log(max_set_len)*max_set_len/log(max_set_len)*n;
    printf("[INFO] Require %d Byte trusted memory\n",mem_size/8);
    printf("[INFO] Require %d KByte trusted memory\n",mem_size/8000);
    printf("[INFO] Require %d MByte trusted memory\n",mem_size/8000000);

    size_t required_trust_mem = mem_size/8000000;
    size_t allowed_maximum = 50;
    size_t batch = required_trust_mem/allowed_maximum + 1;
    printf("[INFO] Need to call trust memory %d times \n",batch);
    printf("[INFO] Each Require %d MByte trusted memory\n",required_trust_mem/batch);


    
    gen_test(n, mode, max_set_len, size_final, X, len, intersection_set);
    /********************************************Generate the random set with the specific parameter End ***************************/

    /*
    *   Server side protocol start
    */
    //server_core(argc,(const char**) argv,X,len);

    out=new size_t[max_set_len];

    /*
    *   Hashing to bucket variable praparation
    */
    size_t n_bucket=max_set_len/log(max_set_len);
    // handy seted value, modified if any needed
    size_t hashfunction_standardivation_ratio = 10; 
    size_t n_max_elements = hashfunction_standardivation_ratio * log(max_set_len);

    

    size_t **len_bucket = new size_t*[n];
    for(size_t i=0;i<n;i++){
        len_bucket[i] = new size_t[n_bucket];
        memset(len_bucket[i], 0, sizeof(size_t)*n_bucket);
    }

    size_t ***bucket = new size_t **[n];
    for(size_t i=0;i<n;i++)
    {
        bucket[i] = new size_t *[n_bucket];
        for(size_t j=0;j<n_bucket;j++){
            bucket[i][j] = new size_t [n_max_elements];
            memset(bucket[i][j], 0, sizeof(size_t)*n_max_elements);
        }
    }



    

    /*
    *   Hashing to bucket part
    */
    printf("[INFO] there are %d buckets!\n",n_bucket);


    for(size_t i=0;i<n;i++)
    {
        //printf("[DEBUG]max_set_len is %d and len[i] is %d\n",max_set_len,len[i]);//把len定义为全局变量，解决形参实参的问题
       hash2bucket(X[i], len[i], n_bucket, len_bucket[i], bucket[i], n_max_elements);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
    }                                                                                                                                                                       
    

    find_min_change(bucket,len_bucket,n,n_bucket,max_set_len);

    /*
    *test hash2bucket 
    */
    // for(int j=0;j<n_bucket;j++)
    // {
    //     for(int k=0;k<len_bucket[0][j];k++)
    //         printf("bucket[0][%d][%d]: %d ",j,k,bucket[0][j][k]);
    //     printf("\n");
    //     for(int k=0;k<len_bucket[1][j];k++)
    //         printf("bucket[1][%d][%d]: %d",j,k,bucket[1][j][k]);
    //     printf("\n");
    // }

    // size_t **y=new size_t *[n_bucket];
    // for(int i=0;i<n_bucket;i++)
    //     y[i]=new size_t[len_bucket[0][i]];

    Timer ts;
    ts.start();
    /*************************************************   Enclave program Start   ****************************************/
    /*
    *@Functionality: do core computation in SGX
    *   @Params:
    *       @X(bucket) : the array of element of each members' each buckets
    *       @len_bucket : the counter to keep the number of elements in each bucket for each member
    *       @out:the output shows what elements they have in common
    *       @n:the number of members
    *       @len_min(max_set_len):the length of the each member(in mod 0)
    *       @n_bucket:the number of bucket each member have(in mod 0)
    */
    msi(global_eid,bucket,len_bucket,out,n,max_set_len,n_bucket);
    /*************************************************   Enclave program End     ****************************************/
    double cost = ts.elapsed();
    printf("The total time cost in sgx is %lf\n", cost);

    /*
    * to test the cost without SGX
    */
    // Timer ts3;
    // ts3.start();
    //msi_out(bucket,len_bucket,out,n,max_set_len,n_bucket);
    // double cost3 = ts3.elapsed();
    // printf("The total time cost out sgx is %lf\n", cost3);


    /*
    *   test output code, comment in non-debug state
    */

    int samenumber=0;
    for(size_t i=0;i<max_set_len;i++)
    {
        if(out[i]!=0)
            samenumber++;
        //printf("%d ",out[i]);
    }
    printf("[INFO] outer intersection result :%d\n",samenumber);

    /* 
    *   Destroy the enclave 
    */
    sgx_destroy_enclave(global_eid);
    
    /*
    *   recycle part 
    */
    delete out;
    for(int i=0; i<n; i++){
        delete X[i];
        delete len_bucket[i];
        for(size_t j=0; j<n_bucket; j++){
            delete bucket[i][j];
        }
        delete bucket[i];
    }

    delete len_bucket;
    delete bucket;
    delete X;
    delete len;

    double cost_main = ts_main.elapsed();
    printf("The total time cost is %lf\n", cost_main);

    return 0;
}

