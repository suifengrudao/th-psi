/*************************************************************************
* test.h
* test suite for the validty of core functionality
*
* Author: stoneboat@mail.nankai.edu.cn 
* (C) 2019 University of NKU. Free for used
***********************************************************************/
#include <iostream>
#include <random>
#include <set>

/*
* @Functionality: generate the test set samples
*
* @Outer-Params:
*		@n_party : number of participants
*		@len_mode : size of the set obeying the certain distribution; this is an enumerate variable 
*		@size_final : size of intersection set 
*		@X : each row of which is a set with size of len[i]
*		@len:
*/

enum set_length_mode{size_avg_mode, size_unbalanced_mode, size_rnd_mode};
void gen_test(const size_t n, const set_length_mode mode, const size_t max_set_len, const size_t size_final, size_t** X, size_t* len, std::set<size_t>& intersection_set);

/*
* @Functionality: assure that the set X[0] has the minimal size
*
*/
//size_t find_min_change(size_t **X,size_t *len,size_t n);

/*
* @Functionality: assertion functionality
*/
void custom_assert(const bool statement);


/*
*	@Functionality: hashing $(x) elements to $(bucket) which has size of $(n_bucket)
*	@Params:
*		@x : the array of element with size of $(len)
*		@count : the counter to keep the number of elements in each bucket of $(bucket)
*/
void hash2bucket(size_t *x, const size_t& len, const size_t& n_bucket, size_t* counter, size_t ** bucket, const size_t& n_max);

size_t myhash(size_t a);