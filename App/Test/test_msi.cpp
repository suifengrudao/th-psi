/*************************************************************************
* test.cpp
* test suite for the validty of core functionality
*
* Author: stoneboat@mail.nankai.edu.cn 
* (C) 2019 University of NKU. Free for used
***********************************************************************/

#include "test_msi.h"

#include <cassert>
#include<cmath>

using namespace std;

/*
* @Functionality: generate the test set samples
*
* @Outer-Params:
*		@n_party : number of participants
*		@len_mode : size of the set obeying the certain distribution; this is an enumerate variable 
*		@size_final : size of intersection set; if size_final == 0 means no predetermination
*		@X : each row of which is a set with size of len[i]
*		@len:
*/
void gen_test(const size_t n, const set_length_mode mode, const size_t max_set_len, const size_t size_final, size_t** X, size_t* len, set<size_t>& intersection_set){
	assert(10<0);
	/*
	*	variable initialization
	*/
	// if (len != NULL){
	// 	delete len;
	// }
	//len = new size_t[n];
	std::uniform_int_distribution<> dis(1, max_set_len);
	intersection_set.clear();


	// parameter for size_rnd_mode
	std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<> dis_len(size_final+1, max_set_len);


    // parameter for size_unbalanced_mode
    const double unbalanced_ratio = 1000;
    std::uniform_int_distribution<> dis_once(1, n-1);
    size_t n_small_party = dis_once(gen);
    assert(size_final < max_set_len/unbalanced_ratio);
    std::uniform_int_distribution<> dis_small(size_final+1, max_set_len/unbalanced_ratio);
    std::uniform_int_distribution<> dis_big(max_set_len/unbalanced_ratio, max_set_len);


	/*
	*	set the size of each set
	// */
	switch(mode){
		case 2:
			for(size_t i=0; i<n; i++){
				len[i] = dis_len(gen);
			}
			break;
		case 1:
			for(size_t i=0; i<n_small_party; i++){
				len[i] = dis_small(gen);
			}
			for(size_t i=n_small_party; i<n; i++){
				len[i] = dis_big(gen);
			}
			break;
		case 0:
		default:
			for(size_t i=0; i<n; i++){
				len[i] = max_set_len;
			}
	}
	// for(int i=0;i<n;i++)
	// 	cout<<"[DEBUG]len[i] is"<<len[i]<<endl;

	/*
	*	generate the fake intersection set
	*/
	if(size_final != 0){
		for(size_t i=0; i<size_final; i++){
			intersection_set.insert(dis(gen));
		}
	}


	/*
	*	generate the array of set X
	*/

	std::set<size_t>::iterator it = intersection_set.begin();  
	for(size_t party = 0; party<n; party++){
		X[party] = new size_t[len[party]];
		it = intersection_set.begin();
		for(int i=0; i<size_final; i++){
			X[party][i] = *(it);
			it++;
		}
		for(int i=size_final; i<len[party]; i++){
			X[party][i] = dis(gen);
		}
	}




	/*
	*	test output code, comment in non-debug state
	*/
	cout<<"[INFO] there are "<< n << " participants, each have a set with size of \t";
	for(size_t i=0; i<n; i++){
		cout<<len[i]<<" ";
	}
	cout<<endl;
	cout<<"[INFO] the testing set size mode is ";
	switch(mode){
		case size_rnd_mode:
			cout<< " size_rnd_mode";
			break;
		case size_unbalanced_mode:
			cout<< " size_unbalanced_mode";
			break;
		case size_avg_mode:
		default:
			cout<< " size_avg_mode";
	}
	cout<<endl;
}


/*
* @Functionality: assure that the set X[0] has the minimal size
*
*/
// size_t find_min_change(size_t **X,size_t *len,size_t n)
// {
	
// 	size_t min_pos = 0;
// 	for(size_t i=1; i<n; i++){
// 		if(len[i] < len[min_pos]){
// 			min_pos = i;
// 		}
// 	}

// 	/*
// 	*	test output code, comment in non-debug state
// 	*/


// 	if(min_pos){
//      	size_t *temp;
//      	size_t temp_l;
        
//      	temp=X[0];
//      	X[0]=X[min_pos];
//      	X[min_pos]=temp;

//      	temp_l=len[0];
//      	len[0]=len[min_pos];
//      	len[min_pos]=temp_l;     
//     }
//     return len[0];
// }

void custom_assert(const bool statement){
	// if(!statement){
	// 	cout << "[ERROR] assertion statement is fault!\n";
	// }
}


size_t myhash(size_t a)
{
	size_t key=a;
	key = (~key) + (key << 21); 
    key = key ^ (key >> 24);
    key = (key + (key << 3)) +(key << 8); 
    key = key ^ (key >> 14);
    key = (key + (key << 2)) +(key << 4); 
    key = key ^ (key >> 28);
    key = key + (key << 31);

    return key;
}

/*
*	@Functionality: hashing $(x) elements to $(bucket) which has size of $(n_bucket)
*	@Params:
*		@x : the array of element with size of $(len)
*		@count : the counter to keep the number of elements in each bucket of $(bucket)
*/
void hash2bucket(size_t *x, const size_t& len, const size_t& n_bucket, size_t* counter, size_t ** bucket, const size_t& n_max)					
{
	for(int i=0;i<len;i++)																															
	{	
		/*
		*	compuate that element x[i] should be placed into pos'th bucket
		*/																																																																																																																												
		int pos = (myhash(x[i]) % n_bucket);
		bucket[pos][counter[pos]] = x[i];
		counter[pos] ++; 
		/*
		*	We need to resrict the maximum number of element each bucket
		*	so if the number of elements exceed the extremity, we abort it and alarm the circumstance
		*/
		if(counter[pos] >= n_max){
			cout<<"[ERROR] the elements are aborted due to exceed the allowed maximum value! report by function <hash2bucket>\n";
		}
	}
	//printf("try debuging\n");
	
            
}


