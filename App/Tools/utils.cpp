// (C) 2019 University of NKU. Free for used
// Author: stoneboat@mail.nankai.edu.cn


/*
* utils.cpp
*
*/
#include "utils.h"


bool is_directory(const std::string& path)
{
    struct stat sb;
    
    if (stat(path.c_str(), &sb) == 0 && S_ISDIR(sb.st_mode))
    {
        return true;
    }
    return false;
}

bool create_directory(const std::string& path, mode_t mode)
{
    if (mkdir(path.data(),mode) != 0) {
        return false;
    }
    return true;
}

