// (C) 2019 University of NKU. Free for used
// Author: stoneboat@mail.nankai.edu.cn

#pragma once

/*
* utils.h
*
*/
#include <array>
#include <string>
#include <sstream>
#include <cstdint>
#include <map>
#include <sys/stat.h>
#include <sys/types.h>

#include <openssl/rand.h>
#include <openssl/err.h>

namespace sgx_mpsi {
    // use for multi-thread communication
    constexpr int port_increase = 150;
    constexpr int max_TransferBytes = 5000000;

    class randomSketchGenerator{
    private:
        bool initialized;
        unsigned char key[32];
    public:
        randomSketchGenerator() {
          RAND_poll();
          initialized = true;
        }

        void reseed() {
          int written = RAND_bytes(key, sizeof(key));
          RAND_seed(key, written);
        }

        bool random_bytes(const size_t &byte_count, unsigned char* out)
        {
          int rc = RAND_bytes(out, byte_count);
          unsigned long err = ERR_get_error();

          if(rc != 0 && rc != 1) {
            fprintf(stderr, "Generating Random Bytes failed, err = 0x%lx\n", err);
            return false;
          }

          return true;
        }
    };

}

/*
* files operation
*/
bool is_directory(const std::string& path);
bool create_directory(const std::string& path, mode_t mode);

